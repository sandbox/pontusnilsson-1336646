<?php
/**
* @file
* Definition of the 'link' panel style.
*/

// Plugin definition
$plugin = array(
  'title' => t('Link'),
  'description' => t('Display a pane or region with a link around it.'),
  'render region' => 'panels_link_style_render_region',
  'render pane' => 'panels_link_style_render_pane',
  'settings form' => 'panels_link_style_settings_form',
  'pane settings form' => 'panels_link_style_settings_form',
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'edit form' => 'panels_link_style_settings_form',
);

/**
* Settings form callback.
*/
function panels_link_style_settings_form($settings) {
  $items = !empty($settings['items']) ? $settings['items'] : 1;
  $form['fields'] = array('#tree' => TRUE);
  for($i = 0; $i < $items; $i++) {
    $form['fields']['link_style_field_' . $i] = array(
      '#type' => 'select',
      '#title' => t('Link field'),
      '#description' => t('Field to retrieve link from'),
      '#default_value' => !empty($settings['fields']['link_style_field_' . $i]) ? $settings['fields']['link_style_field_' . $i] : '',
      '#options' => panels_link_style_fields(),
    );
  }
  // Number of fields, TODO replace functionality with AHAH
  $form['items'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of field items'),
    '#size' => 2,
    '#default_value' => $items,
  );

  return $form;
}

/**
* Render callback.
*
* @ingroup themeable
*/
function theme_panels_link_style_render_pane($vars) {
  $link_style_url = panels_link_style_get_url($vars);
  if ($link_style_url) {
    $css_class = '';
    if (!empty($vars['pane']->css['css_class'])) {
      $css_class = $vars['pane']->css['css_class'];
    }
    return panels_link_style_link(render($vars['content']->content), $link_style_url['url'], $link_style_url['target'], $css_class);
  }
  else {
    return render($vars['content']->content);
  }
}

/**
 * Helper function for linking depending on target
 */
function panels_link_style_link($content, $url, $target = '', $class = ''){
  $url = drupal_parse_url($url);
  if (!empty($target)) {
    return l(($content), $url['path'], array('html' => TRUE, 'attributes' => array('target' => $target, 'class' => array('panels-link-style', $class)), 'query' => $url['query']));
  }
  else {
    return l(($content), $url['path'], array('html' => TRUE, 'attributes' => array('class' => array('panels-link-style', $class)), 'query' => $url['query']));
  }
}

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_link_style_render_region($vars) {  
  $output = '';
  $output .= '<div class="region region-' . $vars['region_id'] . '">';
  $link_style_url = panels_link_style_get_url($vars);
  if (!empty($link_style_url)) {
    $output .= panels_link_style_link(implode('', $vars['panes']), $link_style_url['url'], $link_style_url['target']);    
  }
  else {
    $output .= implode('', $vars['panes']);
  }

  $output .= '</div>';
  return $output;
}

/**
 * Helper function to extract the URL of the link field
 */
function panels_link_style_get_url($vars) {
  if (isset($vars['settings']['fields'])) {
    $display = $vars['display'];
    $context_key = key($display->context);
    $node = $display->context[$context_key]->data;
    foreach ($vars['settings']['fields'] as $link_field) {
      list($entity_type, $field_name) = explode(':', $link_field, 2);
      // Load the entity type's information for this field.
      $ids = entity_extract_ids($entity_type, $node);
      $info = field_info_instance($entity_type, $field_name, $ids[2]);
      $items = field_get_items('node', $node, $field_name);
      if(!empty($items)) {
        // Handle entity reference fields        
        if ($info['widget']['module'] == 'entityreference') {
          if ($info['entity_type'] == 'node') {
            return array('url' => 'node/'.$items['0']['target_id'], 'target' => '');
          }
        }
        if($info['widget']['module'] == 'node_reference') {
          return array('url' => 'node/'.$items['0']['nid'], 'target' => '');
        }

        // Handle "URL" fields.
        if ($info['widget']['module'] == 'url') {
          return array('url' => $items['0']['value'], 'target' => '');
        }

        // Handle link
        if($info['widget']['module'] == 'link') {
          $target = '';
          if (isset($items['0']['attributes']['target'])) {
            $target = $items['0']['attributes']['target'];
          }
        return array('url' => $items['0']['url'], 'target' => $target);
        }
      }
    }
  }
  return FALSE;
}

/**
 * Retrieve fields
 */
function panels_link_style_fields() {
  $types = array();
  $entities = entity_get_info();
  foreach ($entities as $entity_type => $entity) {
    foreach ($entity['bundles'] as $type => $bundle) {
      foreach (field_info_instances($entity_type, $type) as $field_name => $field) {
        $types[$entity_type . ':' . $field_name] = $entity_type . ':' . $field_name;
      }
    }
  }
  return $types;
}
